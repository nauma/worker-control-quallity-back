module.exports = {
  // rest gateway
  server: {
    ip: '0.0.0.0',
    post: 3535
  },

  // mongodb
  database: {
    link: 'mongodb://localhost:27017/worker_control',
    options: { useNewUrlParser: true, useUnifiedTopology: true }
  }
}