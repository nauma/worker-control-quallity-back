# Server

### install
Для установки всех зависимостей, запустите команду:

`$ npm install`

### config
Настройки moleculer `configs/moleculer.dev.config.js`
Настройки сервера `configs/application.dev.config.js`

### running

Запуск сервера, команда:

`$ npm run dev`

#### authorization
Что-бы авторизироваться в системе сделайте запрос на:

`POST /api/authorization/login`

И отправьте JSON

```json
{
	"email": "emailtest@mail.ru",
	"password": "123456789",
}
```

#### register
Что-бы зарегестрировать пользователя сделайте запрос на:

`POST /api/authorization/register`

И отправьте JSON

```json
{
	"first_name": "Name",
	"last_name": "Last name",
	"email": "emailtest@mail.ru",
	"password": "123456789",
	"re_password": "123456789"
}
```