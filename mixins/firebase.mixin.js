const firebase = require('firebase-admin')
const { ValidationError } = require('moleculer').Errors
// 
module.exports = {
	name: 'firebase',
	// 
	settings: {
		firebase: {
			credential: '../configs/firebase.workercontrolsystem',
			databaseURL: 'https://workercontrolsystem.firebaseio.com'
		}
	},
	// 
	actions: {
		'account.get': {
			params: {
				id: { type: 'string', optional: true, default: null }
			},

			async handler (ctx) {
				const { id } = ctx.params
				// 
				if (id) {
					return await this.$fs.collection('accounts').doc(id).get()
						.then(snapshot => {
							if (!snapshot.exists) {
								return null
							}
							// 
							return {
								id: snapshot.id,
								...snapshot.data()
							}
						})
				}
				// 
				return await this.$fs.collection('accounts')
					.get()
					.then(snapshot => {
						const result = []
						// 
						snapshot.forEach(doc => {
							result.push({
								id: doc.id,
								...doc.data()
							})
						})
						// 
						return result
					})
			}
		},

		'account.put': {
			params: {
				first_name: 'string',
				last_name: 'string',
				email: 'string',
				password: 'string',
				tariff: { type: 'string', optional: true, default: 'default' },
				created: { type: 'number', optional: true, default: Number(new Date()) },
				updated: { type: 'number', optional: true, default: Number(new Date()) },
			},

			async handler (ctx) {
				const { first_name, last_name, email, password, tariff, created, updated } = ctx.params
				// 
				return await this.$fs.collection('accounts').add({
					first_name,
					last_name, email,
					password,
					tariff,
					created,
					updated
				})
			}
		},

		'account.update': {
			params: {
				id: 'string',
				first_name: { type: 'string', optional: true, default: null },
				last_name: { type: 'string', optional: true, default: null },
				email: { type: 'string', optional: true, default: null },
				password: { type: 'string', optional: true, default: null },
				tariff: { type: 'string', optional: true, default: null },
				updated: { type: 'number', optional: true, default: Number(new Date()) },
			},

			async handler (ctx) {
				const { id, first_name, last_name, email, password, tariff, updated } = ctx.params
				// 
				const query = { updated }

				if (first_name) query.first_name = first_name
				if (last_name) query.last_name = last_name
				if (email) query.email = email
				if (password) query.password = password
				if (tariff) query.tariff = tariff
				// 
				return await this.$fs.collection('accounts').doc(id).update(query)
			}
		},

		'session.get': {
			params: {
				token: { type: 'string', optional: true, default: null },
				userId: { type: 'array', optional: true, default: null }
			},

			async handler (ctx) {
				const { token, userId } = ctx.params
				// 
				if (!token && !userId) {
					throw new ValidationError('Parameters validation error!', 422, 'VALIDATION_ERROR', { required: true, message: `The 'token' or 'userId' field is required.`, field: "token,userId", })
				}
				// 
				if (token) {
					return await this.$fs.collection('sessions')
						.where('token', '==', token)
						.get()
						.then(snapshot => {
							if (snapshot.empty) {
								return null
							}
							// 
							let result = null
							// 
							snapshot.forEach(doc => {
								result = {
									id: doc.id,
									...doc.data()
								}
							})
							// 
							return result
						})
				}
				// 
				return await this.$fs.collection('sessions')
					.where('userId', '==', userId)
					.get()
						.then(snapshot => {
							const result = []
							// 
							snapshot.forEach(doc => {
								result.push({
									id: doc.id,
									...doc.data()
								})
							})
							// 
							return result
						})
			}
		},

		'session.put': {
			params: {
				token: 'string',
				userId: 'string',
				type: { type: 'string', optional: true, default: 'user' },
				created: { type: 'number', optional: true, default: Number(new Date()) },
				updated: { type: 'number', optional: true, default: Number(new Date()) },
				age: { type: 'number', optional: true, default: 3600 * 24 * 7 }
			},

			async handler (ctx) {
				const { userId, token, type, created, updated, age } = ctx.params
				// 
				return await this.$fs.collection('sessions').add({
					userId,
					token,
					type,
					created,
					updated,
					age
				})
			}
		},
	},
	// 
	async created () {
		if (firebase.apps.length < 1) {
			const { credential, databaseURL } = this.settings.firebase
			// 
			try {
				await firebase.initializeApp({
					credential: firebase.credential.cert(require(credential)),
					databaseURL,
				})
			} catch (e) {}
		}
		// 
		this.$db = firebase.database()
		this.$fs = firebase.firestore()
	}
}