const mongoose = require('mongoose')
// 
const { ValidationError } = require('moleculer').Errors
// 
const usersModel = require('../database/users.model')
const userSessionsModel = require('../database/user_sessions.model')
const objectsModel = require('../database/objects.model')
// 
const { database } = require('../configs/application.dev.config')
// 
module.exports = {
	name: 'mongoose',
	// 
	settings: {
		mongoose: {
      link: database.link,
      options: database.options
		}
	},
	// 
	async created () {
		const { mongoose: config } = this.settings
		// 
		await mongoose.connect(config.link, config.options);
		// 
		this.$db = {
			users: usersModel,
			userSessions: userSessionsModel,
			objects: objectsModel,
		}
		// 
		this.logger.info('Service Mongoose is started!')
	},

	async stopped () {
		await mongoose.disconnect()
		// 
		this.logger.info('Service Mongoose is stopped!')
	}
}