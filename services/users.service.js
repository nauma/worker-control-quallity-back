const { MoleculerError } = require('moleculer').Errors
// 
const mongooseMixin = require('../mixins/mongoose.mixin')
// 
module.exports = {
	name: 'users',
	// 
	settings: {},
	// 
	mixins: [mongooseMixin],
	// 
	methods: {
		generateToken (length = 64) {
			const characters = '_ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_'
			const charactersLength = characters.length
			// 
			let result = ''
			// 
			for (let i = 0; i < length; i++) {
				result += characters.charAt(Math.floor(Math.random() * charactersLength));
			}
			// 
			return result;
		},

		async createSession (user_id, type = 'user', age = 3600 * 24 * 7, token = this.generateToken()) {
			await this.$db.userSessions.create({
				user_id,
				token,
				type,
				age,
				expired: Number(new Date()) + age
			})
			// 
			return { user_id, token }
		}
	},
	// 
	actions: {
		'checkSession': {
			params: {
				token: 'string'
			},

			async handler (ctx) {
				const { token } = ctx.params
				// 
				const session = await this.$db.userSessions.findOne({ token })
				// 
				if (session !== null) {
					if (Number(new Date(session.updated)) + session.age > +(new Date())) {
						return session.user_id
					} else {
						session.remove()
					}
				}
				//
				throw new MoleculerError('Authorization error', 401, 'CLIENT_ERROR', { message: 'Token is invalid' })
			}
		},

		'login': {
			params: {
				email: 'string',
				password: 'string'
			},

			async handler (ctx) {
				const { email, password } = ctx.params
				// 
				const user = await this.$db.users.findOne({ email, password })
				// 
				if (!user) {
					throw new MoleculerError('Authorization error', 401, 'CLIENT_ERROR', { message: 'Email or password is invalid' })
				}
				// 
				const result = await this.createSession(user._id)
				// 
				return result
			}
		},

		'register': {
			params: {
				first_name: { type: 'string' },
				last_name: { type: 'string', optional: true, default: '' },
				email: { type: 'string' },
				password: { type: 'string' },
				re_password: { type: 'string' },
			},

			async handler (ctx) {
				const { first_name, last_name, email, password, re_password } = ctx.params
				// 
				if (password !== re_password) {
					throw new MoleculerError('Register error', 400, 'CLIENT_ERROR', { message: 'Passwords is not equal' })
				}
				// 
				await this.$db.users.create({ first_name, last_name, email, password })
				// 
				const user = await this.$db.users.findOne({ email })
				// 
				const session = await this.createSession(user._id)
				// 
				return session
			}
		},

		'get': {
			params: {
				id: 'string'
			},

			cache: {
				enabled: true,
				keys: ['id'],
				ttl: 20
			},

			async handler (ctx) {
				const _id = ctx.params.id
				// 
				return this.$db.users.findOne({ _id }, { password: 0, _id: 0, __v: 0 })	
			}
		},

		'me': {
			async handler (ctx) {
				const id = ctx.meta.uid
				// 
				return this.broker.call('users.get', { id: String(id) })
			}
		},

		'edit': {
			params: {
				first_name: { type: 'string', optional: true, default: null },
				last_name: { type: 'string', optional: true, default: null },
				password: { type: 'string', optional: true, default: null },
				re_password: { type: 'string', optional: true, default: null },
			},

			async handler (ctx) {
				const user_id = ctx.meta.uid
				const { first_name, last_name, password, re_password } = ctx.params
				// 
				const query = {}

				if (first_name) query.first_name = first_name
				if (last_name) query.last_name = last_name
				if (password && re_password && password === re_password) query.password = password
				// 
				return this.$db.users.findOneAndUpdate({ user_id }, query, { new: true })
			}
		}
	},
	// 
	async created () {
	},

	async started () {

	},

	async stopped () {
		
	}
}