const { MoleculerError } = require('moleculer').Errors
// 
const mongooseMixin = require('../mixins/mongoose.mixin')
//
module.exports = {
	name: 'objects',
	// 
	mixins: [mongooseMixin],
	settings: {},
	// 
	methods: {},
	// 
	actions: {
		'get': {
			params: {
				object_id: { type: 'string', optional: true, default: null }
			},

			async handler (ctx) {
				const _id = ctx.params.object_id
				const user_id = ctx.meta.uid
				// 
				if (_id) {
					return this.$db.objects.findOne({ _id, user_id }, { __v: 0, user_id: 0 })
				}
				// 
				return this.$db.objects.find({ user_id }, { __v: 0, user_id: 0 })
			}
		},

		'add': {
			params: {
				title: 'string',
				description: 'string'
			},

			async handler (ctx) {
				const { title, description } = ctx.params
				const user_id = ctx.meta.uid
				// 
				const object = await this.$db.objects.create({ user_id, title, description })
				// 
				return this.$db.objects.find({ _id: object.id }, { __v: 0, user_id: 0 })
			}
		},

		'edit': {
			params: {
				object_id: 'string',
				title: { type: 'string', optional: true, default: null },
				description: { type: 'string', optional: true, default: null }
			},

			async handler (ctx) {
				const { object_id: _id, title, description } = ctx.params
				const user_id = ctx.meta.uid
				// 
				const query = {}

				if (title) query.title = title
				if (description) query.description = description
				// 
				return this.$db.objects.findOneAndUpdate({ _id, user_id }, query, { new: true })
			}
		},

		'delete': {
			params: {
				object_id: 'string',
			},

			async handler (ctx) {
				const { object_id: _id } = ctx.params
				const user_id = ctx.meta.uid
				// 
				await this.$db.objects.findOneAndRemove({ _id, user_id })
				// 
				return true
			}
		},
	},
	// 
	async created () {

	},

	async started () {

	},

	async stopped () {
		
	}
}