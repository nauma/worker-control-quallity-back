const ApiService = require("moleculer-web");
// 
const { server } = require('../configs/application.dev.config')
// 
module.exports = {
	name: 'api',
	// 
	settings: {
		ip: server.ip,
		port: server.port,
		// 
		path: '/api',
		// 
		routes: [
			{
				path: '/authorization',
				aliases: {
					'POST /login': 'users.login',
					'POST /register': 'users.register',
				},
				onAfterCall
			},
			{
				path: '/web',
				authorization: true,
				bodyParsers: {
					json: true
				},
				aliases: {
					'GET   /account': 'users.me',
					'POST  /account': 'users.edit',
					// 
					'GET  /objects': 'objects.get',
					'POST /objects': 'objects.add',

					'GET  /objects/:object_id': 'objects.get',
					'POST /objects/:object_id': 'objects.edit',
					'DELETE /objects/:object_id': 'objects.delete',
					// 
					'GET  /objects/:object_id/points': 'checkpoints.get',
					'POST /objects/:object_id/points': 'checkpoints.add',
					
					'GET  /objects/:object_id/points/:point_id': 'checkpoints.get',
					'POST /objects/:object_id/points/:point_id': 'checkpoints.edit',
					'DELETE /objects/:object_id/points/:point_id': 'checkpoints.delete',
					// 
					'GET  /objects/:object_id/workers': 'workers.get',
					'POST /objects/:object_id/workers': 'workers.add',

					'GET  /objects/:object_id/workers/:worker_id': 'workers.get',
					'POST /objects/:object_id/workers/:worker_id': 'workers.edit',
					'DELETE /objects/:object_id/workers/:worker_id': 'workers.delete',
					// 
					'GET  /objects/:object_id/reports': 'reports.get',
				},
				onAfterCall
			},

			{
				path: '/mobile',
				bodyParsers: {
					json: true
				},
				aliases: {

				},
				onAfterCall
			}
		],

		onError
	},
	// 
	mixins: [ApiService],
	// 
	dependencies: ['users', 'objects', 'checkpoints', 'workers', 'reports'],
	// 
	methods: {
		async authorize (ctx, route, req, res) {
			// Read the token from header
			let token = req.headers['token'];
			// 
			const id = await this.broker.call('users.checkSession', { token })
			// 
			const account = await this.broker.call('users.get', { id: String(id) })
			// 
			ctx.meta.uid = id
			ctx.meta.account = account
			// 
			return ctx
		}
	}
}
// 
function onAfterCall (ctx, route, req, res, result) {
	return {
		status: true,
		result
	}
}

function onError (req, res, err) {
	console.log(err)
	res.setHeader("Content-Type", "application/json");
	res.writeHead(err.code);
	res.end(JSON.stringify({
		status: false,
		error: {
			message: (err.data && err.data.message) || (err.data && err.data.length > 0 && err.data[0].message) || 'Ошибка сервера'
		}
	}));
}