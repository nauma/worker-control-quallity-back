const mongoose = require('mongoose')
// 
const Name = 'Users'
const Schema = new mongoose.Schema({
	email: {
		type: String,
		required: true,
		unique: true
	},

	password: {
		type: String,
		required: true
	},

	first_name: {
		type: String,
		required: true
	},

	last_name: {
		type: String,
		default: ''
	},
}, {
	timestamps: {
		createdAt: 'created',
		updatedAt: 'updated'
	}
})
// 
module.exports = mongoose.model(Name, Schema)