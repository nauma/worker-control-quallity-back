const mongoose = require('mongoose')
// 
const Name = 'Objects'
const Schema = new mongoose.Schema({
	user_id: {
		type: mongoose.Types.ObjectId,
		required: true
	},

	title: {
		type: String,
		required: true
	},

	description: {
		type: String,
		default: ''
	},

	enabled: {
		type: Boolean,
		default: true
	}
}, {
	timestamps: {
		createdAt: 'created',
		updatedAt: 'updated'
	}
})
// 
module.exports = mongoose.model(Name, Schema)