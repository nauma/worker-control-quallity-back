const mongoose = require('mongoose')
// 
const Name = 'UserSessions'
const Schema = new mongoose.Schema({
	user_id: {
		type: mongoose.Types.ObjectId,
		required: true
	},

	age: {
		type: Number,
		default: 3600
	},

	expired: {
		type: Date,
		required: true
	},

	token: {
		type: String,
		requried: true
	},
}, {
	timestamps: {
		createdAt: 'created',
		updatedAt: 'updated'
	}
})
// 
module.exports = mongoose.model(Name, Schema)